<?php

namespace App\Controller;


use App\Entity\Message;
use App\Entity\Utilisateur;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;


class HomeController extends AbstractController 
{
    /**
     * @Route("/accueil", name="accueil")
     */
    public function index()
    {
       
        return $this->render('accueil/accueil.html.twig', [
            
        ]);
    }

    /**
     * @Route("/category", name="category")
     */
    public function category()
    {
       
        return $this->render('accueil/category.html.twig', [
            
        ]);
    }


/**
     * @Route("/blog", name="blog")
     */
    public function blog()
    {
       
        return $this->render('accueil/blog.html.twig', [
            
        ]);
    }



    /**
     * @Route("/contact", name="contact", methods={"GET","POST"})
     */
    public function contact(Request $request)
    {

        if(($request->server->get("REQUEST_METHOD")=="POST")AND($request->request->count()>1)){

            $nom=$request->request->get("nom");
            $email=$request->request->get("email");
            $objet=$request->request->get("objet");
            $message=$request->request->get("message");
            $envois =new Message();
            

            //$envois ->setNom($nom);
            //$envois ->setEmail($email);
            //$envois ->setObjet($objet);
            //$envois ->setMessage($message);
            //OU
            $envois ->setNom($nom) ->setEmail($email) ->setObjet($objet) ->setMessage($message);

            $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($envois);
                $entityManager->flush();
                return $this->render('accueil/accueil.html.twig', [
                ]);
        
        }

       return $this->render('accueil/contact.html.twig', [
            
        ]);
    }



    /**
     * @Route("/inscription", name="inscription", methods={"GET","POST"})
     */
    public function inscription(Request $request)
    {
        if($request->server->get("REQUEST_METHOD")=="POST"){
                
            $nom=$request->request->get("nom");
            $prenom=$request->request->get("prenoms");
            $age=$request->request->get("age");
            $email=$request->request->get("email");
            $pass=$request->request->get("pass");
            $pass= password_hash($pass, PASSWORD_DEFAULT);
            $image=$request->request->get("img");
            $inscrit =new Utilisateur();

            $inscrit ->setNom($nom) ->setPrenoms($prenom) ->setEmail($email) ->setAge($age) ->setImage($image) ->setPass($pass);

            $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($inscrit);
                $entityManager->flush();
        
            //return $this->render('accueil/accueil.html.twig', [
                
            //]);
        }
        return $this->render('accueil/inscription.html.twig', [
                
        ]);


        
        
    }


    /**
     * @Route("/artist", name="artist")
     */
    public function artist()
    {
       
        return $this->render('accueil/artist.html.twig', [
            
        ]);
    }



    /**
     * @Route("/connexion", name="connexion")
     */
    public function connexion()
    {
       
        return $this->render('accueil/connexion.html.twig', [
            
        ]);
    }


    /**
     * @Route("/listmessage", name="listmessage")
     */
    public function listmessage()
    {
       
        return $this->render('accueil/listmessage.html.twig', [
            
        ]);
    }






}


