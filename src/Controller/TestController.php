<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     */
    public function index()
    {
        $nom = ["Produit 1", "Produit 2", "Produit 3"];
        return $this->render('test/index.html.twig', [
            'noms' => $nom ,
        ]);
    }

    /**
     * @Route("/test/{id}", name="produit")
     */
    public function produit($id)
    {
        $nom = ["Produit 1", "Produit 2", "Produit 3"];
        return $this->render('test/produit.html.twig', [
            'noms' => $nom , 'id' => $id ,
        ]);
    }


}